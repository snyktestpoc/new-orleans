### What's Covered for Team Members

* Parking/Transportation to and from home airports up to $80 USD total (this is on top of the limit set for flight costs in your region)
* Transportation to and from New Orleans airport scheduled groups transfers. These have all been arranged. 
  * If you are arriving before May 8th or leaving after May 14th we **won't reimburse you** for your transport to or from the airport unless you have an exception explicitly agreed upon with the Contribute team. 
    * Those coming in for QBR's should mark those transportation expenses for QBR's not Contribute. 
    * If you are coming in early as part of support or planning Contribute you may expenses that transport with the Contribute expense tag. 
  * If you have to leave before May 14th, you may expense up to $35 to get to NOLA airport as long as there are no transfers booked for you. 
  * If you choose to not utilize scheduled transports on your arrival/ departure date you **cannot expense** your transport. 

* Airfare
  * [How to book flights and what's covered is outlined here](https://gitlab.com/gitlabcontribute/new-orleans/blob/master/team-travel.md)
  * If you need to change your flight for any reason outside of a direct request from your manager you may not expense this change. 
* Up to $60 in luggage checking fees total
* Hotel nights/tax have already been paid (each person will need to check in and provide a CC for incidentals, but won't be charged unless there are any, basic internet in the room included)
* GitLab has arranged meals for everyone attending on the following dates: Thursday, May 9th - Monday, May 13th. If you choose to not consume what we have prearranged or go out on your own you cannot expense separate items/ meals. Example: we will have coffee at the hotel every day if you decide you need a Starbucks that will not be expensable. 
* GitLab will have a sponsored onsite bar at Vitascope & 8Block on the 3rd floor of the Hyatt. 
  * We will cover well drinks plus a couple specialty cocktails each night from 8pm to midnight, only at the hotel bar - 8Block/Vitascope on the 3rd floor 
  * If you order something outside of these hours or off menu these drink cannot be expensed or put on the GitLab tab. 
  *   If your team has a happy hour or drinks with **your team dinner that will be handled by your team lead**- please ask them for their parameters on ordering. 
* For travelers: you may expense up to $95 towards meals combined on arrival & departure days. This $95 is for GitLab Team only.  

### What's **NOT** Covered

* Food on any additional days you stay outside the program or offsite (apart from the team dinners)
* Room service, mini/bar, late night eats/drinks, etc. (i.e. food and beverage outside of what is officially being provided during Contribute)
* Transportation costs that aren't to/from the airport and hotel (e.g. do not expense cab rides from after hour activities back to the hotel)
* Laundry
* Incidentals at the hotel (each person needs to put down their own card)
* Any snacks or souvenirs you pick up during excursions or offsite events unless provided by our team 
* **Please note if you get anything delivered to the hotel they charge a $25+ handling fee for all packages received.** 
* Anything not listed in the "What's Covered" section. When in doubt, go with the assumption that it can't be expensed, and use your best judgment.

___
We want to ensure everyone has a safe and successful week and that everyone feels like they are getting their needs met, so if you have any questions about the policies above please reach out - [how to is found here ](https://gitlab.com/gitlabcontribute/new-orleans/blob/master/README.md#questions)